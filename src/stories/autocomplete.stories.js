import Autocomplete from '../components/Autocomplete';

export default {
    component: Autocomplete,
    title: 'Autocomplete',
};

export const normalRequest = () => ({
    components: {Autocomplete},
    data: () => ({
        post_id: 111,
        post_name: 'Hello World'
    }),
    template: `
        <div>
            <Autocomplete
                :url="\`/mocks/users.json\`"
                :current_id.sync="post_id"
                :current_name="post_name" />
            {{ post_id }}: {{ post_name }}
        </div>
    `
})
