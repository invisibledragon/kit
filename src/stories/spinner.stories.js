import Spinner from '../components/Spinner';

export default {
    component: Spinner,
    title: 'Spinner'
};

export const normalUsage = () => ({
    components: {Spinner},
    template: `
        <Spinner />
    `
})

