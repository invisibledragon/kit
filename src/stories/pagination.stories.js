import List from '../components/List';
import Loader from '../components/Loader';

export default {
    component: List,
    title: 'List'
};

export const normalUsage = () => ({
    components: {Loader, List},
    data: () => ({
        url: '/mocks/users.json',
    }),
    methods: {
        setUrl(url) {
            this.url = url;
        }
    },
    template: `
        <div>
            {{ url }}
            <Loader :url="url">
                <List slot-scope="d" v-bind:data="d.data" v-on:navigate="setUrl">
                    <template slot-scope="x">
                        <div class="alert alert-info" v-for="user in x.results">
                            {{ user.name }}
                        </div>
                    </template>
                </List>
            </Loader>
        </div>
    `
})

