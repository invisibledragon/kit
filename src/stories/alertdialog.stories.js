import AlertDialog from '../components/AlertDialog';
import { ShowModal } from '../components/SimpleModal';

export default {
    component: AlertDialog,
    title: 'AlertDialog'
};

export const simpleOK = () => ({
    components: {AlertDialog},
    created: function(){
        this.$nextTick(function(){
            ShowModal("Are you sure you want to pet the dog?").then(function(){
                alert("Dog was petted");
            });
        });
    },
    template: `
        <AlertDialog />
    `
});

export const infoBox = () => ({
    components: {AlertDialog},
    created: function(){
        this.$nextTick(function(){
            ShowModal({
                "title": "Please Confirm",
                "text": "Are you sure you want to pet the dog?",
                "icon": "warning"
            }).then(function(){
                alert("Dog was petted");
            });
        });
    },
    template: `
        <AlertDialog />
    `
});

