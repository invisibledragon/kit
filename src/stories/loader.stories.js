import Loader from '../components/Loader';

export default {
    component: Loader,
    title: 'Loader',
};

export const normalRequest = () => ({
    components: {Loader},
    template: `
        <Loader url="http://httpbin.org/get">
            <template slot-scope="x">
                <div class="bg-info m-4 p-4">
                    <p>Origin {{ x.data.origin }}</p>
                    <p>Headers</p>
                    <p v-for="key, header in x.data.headers">
                        <strong v-text="key"></strong>
                        <span v-text="header"></span>
                    </p>
                </div>
            </template>
        </Loader>
    `
})

export const accessDenied = () => ({
    components: {Loader},
    template: `
        <Loader url="http://httpbin.org/status/403">
            <template slot-scope="x">
                <div class="bg-info m-4 p-4">
                    <p>Origin {{ x.data.origin }}</p>
                    <p>Headers</p>
                    <p v-for="key, header in x.data.headers">
                        <strong v-text="key"></strong>
                        <span v-text="header"></span>
                    </p>
                </div>
            </template>
        </Loader>
    `
});
