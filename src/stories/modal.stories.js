import Modal from '../components/Modal';
import { boolean } from "@storybook/addon-knobs";

export default {
    component: Modal,
    title: 'Modal'
};

export const normalUsage = () => ({
    components: {Modal},
    props: {
        show: {
            default: boolean('Show', true)
        }
    },
    template: `
        <Modal :show="show">
            <div class="modal-header">
                Login to my app
            </div>
            <div class="modal-body">
                This is my app
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">
                    This is a button
                </button>
            </div>
        </Modal>
    `
})

