import Form from '../components/Form';
import FormControl from "../components/FormControl";

export default {
    component: Form,
    title: 'Form',
};

export const normalRequest = () => ({
    components: {Form, FormControl},
    data: () => ({
        post: { title: "chips", text: "cheese" }
    }),
    methods: {
        showSuccess() {
            alert('Success!');
        }
    },
    template: `
        <Form :data="post" url="http://httpbin.org/put" :defaultErrors="{ }" @success="showSuccess">
            <template slot-scope="resp">
                <FormControl
                    name="title"
                    :value.sync="post.title"
                    :errors="resp.errors.title"
                    label="Post Title" />
                <FormControl
                    name="text"
                    :value.sync="post.text"
                    :errors="resp.errors.text"
                    label="Post Text" />
                <button type="submit" class="btn btn-primary">Submit de form</button>
            </template>
        </Form>
    `
})

export const badRequest = () => ({
    components: {Form, FormControl},
    data: () => ({
        post: { title: "chips", text: "cheese" }
    }),
    methods: {
        showSuccess() {
            alert('Success!');
        }
    },
    template: `
        <Form :data="post" url="/mocks/users.json" :defaultErrors="{ }" @success="showSuccess">
            <template slot-scope="resp">
                <FormControl
                    name="title"
                    :value.sync="post.title"
                    :errors="resp.errors.title"
                    label="Post Title" />
                <FormControl
                    name="text"
                    :value.sync="post.text"
                    :errors="resp.errors.text"
                    label="Post Text" />
                <button type="submit" class="btn btn-primary">Submit de form</button>
            </template>
        </Form>
    `
})
