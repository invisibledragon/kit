import DateTimeInput from '../components/DateTimeInput';

export default {
    component: DateTimeInput,
    title: 'DateTimeInput'
};

export const normalUsage = () => ({
    components: {DateTimeInput},
    data: function(){
        return {
            date: new Date().toISOString()
        }
    },
    template: `
        <div>
            <DateTimeInput :date.sync="date" />
            <div class="alert alert-info" v-text="date"></div>
        </div>
    `
})

