import EventBus from '@/components/EventBus';

export function ShowModal(props) {
    if (typeof props === "string") {
        props = {
            text: props
        };
    }
    EventBus.$emit("alert", props);

    let promise = new Promise(function (resolve) {
        EventBus.$once("alertResult", function (button) {

            if (!button.loading) {
                EventBus.$emit("alertHide");
            }
            resolve({result: button.action, value: button.value});

        });
    });

    return promise;
}

export function HideModal() {
    EventBus.$emit("alertHide");
}
