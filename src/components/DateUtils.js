export function padDate(d) {
    d = d.toString();
    if (d.length === 1) d = "0" + d;
    return d;
}

export function getDaysArray(start, end) {
    // Thank you https://stackoverflow.com/a/50398144/230419 mostly
    let arr = [];
    let dt = new Date(start);
    while (dt.getDate() != end.getDate()) {
        arr.push(new Date(dt));
        dt.setDate(dt.getDate() + 1);
    }
    arr.push(new Date(dt));
    return arr;
}

export function getRows(month, year, obj) {
    let rows = [];
    let row = {days: []};

    let start = new Date(year, month, 1);

    // Prefill first row
    for (let i = start.getDay(); i > 1; i--) {
        start.setDate(start.getDate() - 1);
        row.days.unshift({
            day: start.getDate(),
            month: start.getMonth(),
            number: start.getFullYear() + padDate(start.getMonth()) + padDate(start.getDate()),
            other: true
        });
    }
    obj.earliest = start;

    let end = new Date(year, month + 1, 1);
    end.setDate(0);
    for (let i = 1; i <= end.getDate(); i++) {
        row.days.push({
            day: i,
            month: month,
            number: end.getFullYear() + padDate(end.getMonth()) + padDate(i),
        });
        if (row.days.length === 7) {
            rows.push(row);
            row = {days: []};
        }
    }

    // Fill end of month
    for (let i = end.getDay(); i !== 0; i = end.getDay()) {
        end.setDate(end.getDate() + 1);
        row.days.push({
            day: end.getDate(),
            month: end.getMonth(),
            number: start.getFullYear() + padDate(end.getMonth()) + padDate(end.getDate()),
            other: true
        });
    }
    obj.latest = end;

    rows.push(row);
    return rows;
}