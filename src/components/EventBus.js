import Vue from 'vue';

const EventBus = new Vue({
    data: () => ({
        user: {}
    })
});

export default EventBus;
